# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.3
# (Fill in the missing code) Replace *** in the following code with a
# statement that will print a message like 'Congratulations! Your grade of
# 91 earns you an A in this course'. Your statement should print the value
# stored in the variable grade:
#
# if grade >= 90:
#     ***
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-05-22.

grade = 91

if grade >= 90:
    print('Congratulations! Your grade of ' + str(grade) + ' earns you an'\
        ' A in this course.')

