# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.7:
# (Multiples) Use if statements to determine whether 1024 is a multiple of 4 
# and whether 2 is a multiple of 10. (Hint: Use the remainder operator.)
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-01.

number1 = 1024
number2 = 2

if number1 % 4 == 0:
    print('The number', number1, 'is a multiple of 4.')
if number1 % 4 != 0:
    print('The number', number1, 'is not a multiple of 4.')

if number2 % 10 == 0:
    print('The number', number2, 'is a multiple of 10.')
if number2 % 10 != 0:
    print('The number', number2, 'is not a multiple of 10.')



