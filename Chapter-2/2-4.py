# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.4
# (Arithmetic) For each of the arithmetic operators +, -, *, /, // and **,
# display the value of an expression with 27.5 as the left operand and 2 as
# the right operand.
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-05-23.

a = 27.5
b = 2
print(str(a) + " + " + str(b) + " = " + str(a + b))
print(str(a) + " - "+ str(b) + " = " + str(a - b))
print(str(a) + " * " + str(b) + " = " + str(a * b))
print(str(a) + " / " + str(b) + " = " + str(a / b))
print(str(a) + " // " + str(b) + " = " + str(a // b))
print(str(a) + " ** " + str(b) + " = " + str(a ** b))
