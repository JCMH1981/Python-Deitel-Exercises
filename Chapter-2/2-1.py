# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.1:
# (What does this code do?) Create the variables x = 2 and y = 3, then
# determine what each of the following statements displays:
#     a) print('x =', x)
#     b) print('Value of', x, '+', x, 'is', (x + x))
#     c) print('x =')
#     d) print((x + y), '=', (y + x))
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-05-21.

x = 2
y = 3
print('x =', x)
print('Value of', x, '+', x, 'is', (x + x))
print('x =')
print((x + y), '=', (y + x))
