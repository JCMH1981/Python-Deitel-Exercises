# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.9:
#
# (Integer Value of a Character) Here's a peek ahead. In this chapter, you
# learned about strings. Each of a string's characters has an integer
# representation. The set of characters a computer uses together with the
# characters' integer representations is called that computer's character
# set. You can indicate a character value in a program by enclosing that
# character in quotes, as in 'A'. To determine a character's integer value,
# call the built-in function ord:
#
#     In [1]: ord('A')
#     Out[1]: 65
#
# Display the integer equivalents of B C D b c d 0 1 2 $ * + and the space
# character.
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-03.

print('The integer equivalent of B is', str(ord('B')))
print('The integer equivalent of C is', str(ord('C')))
print('The integer equivalent of D is', str(ord('D')))
print('The integer equivalent of b is', str(ord('b')))
print('The integer equivalent of c is', str(ord('c')))
print('The integer equivalent of d is', str(ord('d')))
print('The integer equivalent of 0 is', str(ord('0')))
print('The integer equivalent of 1 is', str(ord('1')))
print('The integer equivalent of 2 is', str(ord('2')))
print('The integer equivalent of $ is', str(ord('$')))
print('The integer equivalent of * is', str(ord('*')))
print('The integer equivalent of + is', str(ord('+')))
print('The integer equivalent of the space character is', str(ord(' ')))
