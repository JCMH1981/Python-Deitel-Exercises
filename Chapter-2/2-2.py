# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.2:
# (What's wrong with this code?) The following code should read an integer
#into the variable rating:
#
#    rating = input('Enter an integer rating between 1 and 10')
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-05-22.

#Since function input() always returns a string, we need to convert
#the string to an integer using the built-in function int().
rating = int(input('Enter an integer rating between 1 and 10: '))
print(rating)
