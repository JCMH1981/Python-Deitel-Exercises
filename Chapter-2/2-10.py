# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.10:
#
# (Arithmetic, Smallest and Largest) Write a script that inputs three
# integers from the user. Display the sum, average, product, smallest and
# largest of the numbers. Note that each of these is a reduction in
# functional-style programming.
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-06.

number1 = int(input('Input the first integer: '))
number2 = int(input('Input the second integer: '))
number3 = int(input('Enter the third integer: '))

print("The sum of the numbers is", number1 + number2 + number3)
print("The average of the numbers is", (number1 + number2 + number3)/3)
print("The product of the numbers is", number1 * number2 * number3)
print("The smallest of the numbers is", min(number1, number2, number3))
print("The largest of the numbers is", max(number1, number2, number3))


