# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.8:
# (Table of Squares and Cubes) Write a script that calculates the squares and
# cubes of the numbers from 0 to 5. Print the resulting values in table
# format, as shown below. Use the tab escape sequence to achieve the
# three-column output
#
#       number  square  cube
#       0       0       0
#       1       1       1
#       2       4       8
#       3       9       27
#       4       16      64
#       5       25      125
#
# The next chapter shows how to "right align" numbers. You could try that as
# an extra challenge here. The output would be:
#
#       number  square  cube
#            0       0     0
#            1       1     1
#            2       4     8
#            3       9    27
#            4      16    64
#            5      25   125
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-01.

print('number\tsquare\tcube')
print(str(0) + '\t' + str(0*0) + '\t' + str(0*0*0))
print(str(1) + '\t' + str(1*1) + '\t' + str(1*1*1))
print(str(2) + '\t' + str(2*2) + '\t' + str(2*2*2))
print(str(3) + '\t' + str(3*3) + '\t' + str(3*3*3))
print(str(4) + '\t' + str(4*4) + '\t' + str(4*4*4))
print(str(5) + '\t' + str(5*5) + '\t' + str(5*5*5))
