# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.11:
#
# (Separating the Digits in an Integer) Write a script that inputs a
# five-digit integer from the user. Separate the number into its individual
# digits. Print them separated by three spaces each. For example, if the user
# types in the number 42339, the script should print
#
#	4   2   3   3   9
#
# Assume that the user enters the correct number of digits. Use both the
# floor division and remainder operations to "pick off" each digit.
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-10.

number = int(input('Input the five-digit integer: '))

first_digit = number // 10000
first_remainder = number % 10000

second_digit = first_remainder // 1000
second_remainder = first_remainder % 1000

third_digit = second_remainder // 100
third_remainder = second_remainder % 100

fourth_digit = third_remainder // 10
fourth_remainder = third_remainder % 10

fifth_digit = fourth_remainder

three_spaces = '   '

print(str(first_digit) + three_spaces + str(second_digit) + three_spaces +
      str(third_digit) + three_spaces + str(fourth_digit) + three_spaces +
      str(fifth_digit))





