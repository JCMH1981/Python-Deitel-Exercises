# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.15:
# (Sort in Ascending Order) Write a script that inputs three different
# floating-point numbers from the user. Display the numbers in increasing
# order. Recall that an if statement's suite can contain more than one
# statement. Prove that your script works by running it on all six possible
# orderings of the numbers. Does your script work with duplicate numbers?
# [This is challenging. In later chapters you'll do this more conveniently
#  and with many more numbers.]
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-19.

number1 = float(input('Input the first number: '))
number2 = float(input('Input the second number: '))
number3 = float(input('Input the third number: '))

# Minimum number
min_number = number1

if number2 < min_number:
    min_number = number2

if number3 < min_number:
    min_number = number3

# Maximum number
max_number = number1

if number2 > max_number:
    max_number = number2

if number3 > max_number:
    max_number = number3

# Medium number
mid_number = number1

if number2 != min_number:
    if number2 != max_number:
        mid_number = number2

if number3 != min_number:
    if number3 != max_number:
        mid_number = number3

print('The numbers in ascending order are', 
      str(min_number) + ', ' + str(mid_number) + ' and ' + str(max_number))
