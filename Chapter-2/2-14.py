# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.14:
#(Target Heart-Rate Calculator) While exercising, you can use a heart-rate
# monitor to see that your heart rate stays within a safe range suggested by
# your doctors and trainers. According to the American Heart Association
# (AHA) (http://bit.ly/AHATargetHeart-Rates), the formula for calculating
# your maximum heart rate in beats per minute is 220 minus your age in years.
# Your target heart rate is 50–85% of your maximum heart rate. Write a script
# that prompts for and inputs the user's age and calculates and displays the
# user's maximum heart rate and the range of the user's target heart rate.
# [These formulas are estimates provided by the AHA; maximum and target heart
# rates may vary based on the health, fitness and gender of the individual.
# Always consult a physician or qualified healthcare professional before
# beginning or modifying an exercise program.]
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-15.

age = int(input('Input the user\'s age: '))

maximum_heart_rate = 220 - age
print('User\'s maximum heart rate:', maximum_heart_rate, 'BPM')

min_target_heart_rate = 0.5*maximum_heart_rate
max_target_heart_rate = 0.85*maximum_heart_rate
print('Range of the user\'s target heart rate:', 
      str(min_target_heart_rate) + '-' + str(max_target_heart_rate), 'BPM')

