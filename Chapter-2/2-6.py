# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.6:
# (Odd or Even) Use if statements to determine whether an integer is odd or
# even. [Hint: Use the remainder operator. An even number is a multiple of 2.
# Any multiple of 2 leaves a remainder of 0 when divided by 2.]
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-05-27.

number = int(input('Input the integer: '))

if number % 2 == 0:
    print('The number', number, 'is an even integer.')
if number % 2 != 0:
    print('The number', number, 'is an odd integer.')



