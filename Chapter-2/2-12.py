# Intro to Python for Computer Science and Data Science 2020, Deitel & Deitel
#
# Solution of exercise 2.12:
# (7% Investment Return) Some investment advisors say that it's reasonable to 
# expect a 7% return over the long term in the stock market. Assuming that you
# begin with $1000 and leave your money invested, calculate and display how 
# much money you'll have after 10, 20 and 30 years. Use the following formula 
# for determining these amounts:
#       a = p(1 + r)^n
# where
#       p is the original amount invested (i.e., the principal of $1000),
#       r is the annual rate of return (7%),
#       n is the number of years (10, 20 or 30) and
#       a is the amount on deposit at the end of the nth year.
#
# Written by Juan Carlos Moreno (jcmhsoftware@gmail.com), 2023-06-14.

p = 1000
print('Original amount invested:', p)

r = 0.07
print('Annual rate of return:', r*100, '%')

a1 = p*(1 + r)**10
print('Amount on deposit at the end of the ' + str(10) + 'th year:', a1)

a2 = p*(1 + r)**20
print('Amount on deposit at the end of the ' + str(20) + 'th year:', a2)

a3 = p*(1 + r)**30
print('Amount on deposit at the end of the ' + str(30) + 'th year:', a3)



